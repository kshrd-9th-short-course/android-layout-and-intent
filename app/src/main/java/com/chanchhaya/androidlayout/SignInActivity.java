package com.chanchhaya.androidlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = SignInActivity.class.getName();
    public static final String EXTRA_EMAIL = "com.chanchhaya.androidlayout.EMAIL";
    public static final String EXTRA_PASSWORD = "com.chanchhaya.androidlayout.PASSWORD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        EditText editEmail = findViewById(R.id.edit_email);
        EditText editPassword = findViewById(R.id.edit_password);
        Button btnSignIn = findViewById(R.id.btn_sign_in);

        Intent intent = new Intent(this, UserProfileActivity.class);

        btnSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();

                // Print
                System.out.println("Email = " + email);
                System.out.println("Password = " + password);

                // Logcat
                Log.e(TAG, email);
                Log.e(TAG, password);

                intent.putExtra(EXTRA_EMAIL, email);
                intent.putExtra(EXTRA_PASSWORD, password);

                startActivity(intent);
            }
        });

    }

}