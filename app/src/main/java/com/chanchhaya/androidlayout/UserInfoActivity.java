package com.chanchhaya.androidlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class UserInfoActivity extends AppCompatActivity {

    // Declare widget/view
    public EditText editEmail;
    public EditText editObjective;

    public static final String EXTRA_EMAIL = "com.chanchhaya.androidlayout.EMAIL";
    public static final String EXTRA_OBJECTIVE = "com.chanchhaya.androidlayout.OBJECTIVE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        // Initialize views
        editEmail = findViewById(R.id.edit_request_email);
        editObjective = findViewById(R.id.edit_objective);

    }

    public void onSendEmail(View view) {

        String email = editEmail.getText().toString();
        String objective = editObjective.getText().toString();

        Intent returnIntent = getIntent();
        returnIntent.putExtra(EXTRA_EMAIL, email);
        returnIntent.putExtra(EXTRA_OBJECTIVE, objective);

        setResult(RESULT_OK, returnIntent);

        finish();

    }

}