package com.chanchhaya.androidlayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MailActivity extends AppCompatActivity {

    // 1. Declare button
    Button btnSendMail;

    // Declare request code
    public static final int REQUEST_CODE = 1;
    public static final String TAG = MailActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);

        // 2. Initialize button by using findViewById(ID)
        btnSendMail = findViewById(R.id.btn_send_mail);

        // 3. Set event on button
        btnSendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO
                // 4. Initialize Intent object
                Intent intent = new Intent(MailActivity.this, UserInfoActivity.class);
                // 5. Call startActivity() method
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // TODO
        if (requestCode == REQUEST_CODE) {
            Log.e(TAG, resultCode + "");
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String requestEmail = data.getStringExtra(UserInfoActivity.EXTRA_EMAIL);
                    String objective = data.getStringExtra(UserInfoActivity.EXTRA_OBJECTIVE);
                    Log.e(TAG, requestEmail + " - " + objective);
                }
            } else {
                Log.e(TAG, "Result is not OKAY");
            }
        }



    }

}