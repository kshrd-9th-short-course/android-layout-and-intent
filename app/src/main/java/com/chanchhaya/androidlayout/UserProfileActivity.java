package com.chanchhaya.androidlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class UserProfileActivity extends AppCompatActivity {

    private static final String TAG = UserProfileActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        TextView textEmail = findViewById(R.id.text_full_name);
        TextView textPassword = findViewById(R.id.text_job_title);

        Intent intent = getIntent();
        String email = intent.getStringExtra(SignInActivity.EXTRA_EMAIL);
        String password = intent.getStringExtra(SignInActivity.EXTRA_PASSWORD);

        if (email != null && password != null) {
            Log.w(TAG, email);
            Log.w(TAG, password);
        }

        textEmail.setText(email);
        textPassword.setText(password);

    }
}